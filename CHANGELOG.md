# Change Log

All notable changes to the "ipela-git-clerk" extension will be documented in this file.

## [1.0.0]
- Ported from Vue to vanilla javascript
- Improved styling
## [0.*]

- Basic functionality completed