# Project Title

## Ipela Git Clerk

Publish git comments in a sensible format.

## Screenshots

![App Screenshot](./assets/ipela-git-clerk.gif)

## Acknowledgements

- [Simple git (for git functionality)](https://github.com/steveukx/git-js)
- [Category List (default list modified from)](https://dev.to/walternascimentobarroso/semantic-versioning-and-changelog-32ad)

## Motivation

I often find that when I'm searching my commit history, it's because I'm stuck on a file and I'll tend to search by filename and not by message.

Using the excellent [Commit Message Editor](https://marketplace.visualstudio.com/items?itemName=adam-bender.commit-message-editor) start proving tedious because I'd have to enter files manually.

## Installation

Install my-project with npm

```bash
  npm install my-project
  cd my-project
```

## Configuration
- Open Settings [Ctrl+,]
- Search for **IpelaGitClerk** or open JSON
- Edit:
``` 
"ipelatech.ipela-git-clerk.categories": [
		{
			"label": "Build",
			"description": "for build-related activities"
		},
		{
			"label": "Added",
			"description": "for adding new code"
		},
		{
			"label": "Changed",
			"description": "for changes in existing code"
		},
		{
			"label": "Removed",
			"description": "for removing code"
		}
	]
```
## License

[MIT](https://choosealicense.com/licenses/mit/)
  
## Contributing

Contributions are always welcome!

  