const vscode = require("vscode");
const fs = require("fs");
const path = require("path");
const simpleGit = require("simple-git");

/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {
	const git = simpleGit(vscode.workspace.workspaceFolders[0].uri.path);
	let current_panel;
	let open_clerk = vscode.commands.registerCommand(
		"ipela-git-clerk.open-clerk",
		function () {
			current_panel = vscode.window.createWebviewPanel(
				"openWebview", // Identifies the type of the webview. Used internally
				"Ipela Git Clerk", // Title of the panel displayed to the user
				vscode.ViewColumn.One, // Editor column to show the new webview panel in.
				{
					// Enable scripts in the webview
					enableScripts: true, //Set this to true if you want to enable Javascript.
					retainContextWhenHidden: true,
				}
			);

			const file_path = vscode.Uri.file(
				path.join(context.extensionPath, "src", "view", "index.html")
			);
			current_panel.webview.html = fs.readFileSync(
				file_path.fsPath,
				"utf8"
			);

			current_panel.onDidDispose(
				() => {
					current_panel = undefined;
				},
				undefined,
				context.subscriptions
			);

			let settings = vscode.workspace.getConfiguration(
				"ipelatech.ipela-git-clerk"
			);

			current_panel.webview.onDidReceiveMessage(
				async (message) => {
					if (message.command === "console") {
						console.log(message.data);
						return;
					}

					console.log("RECEIVE MESSAGE", message);

					if (
						[
							"success",
							"get-git-status",
							"get-tags",
							"get-categories",
						].includes(message.command)
					) {
						vscode.window.showInformationMessage(
							"Git Clerk: Updating File List"
						);
						let response;
						switch (message.command) {
							case "get-git-status":
								//unadded
								let git_status = await git.status();
								//modified
								let modified_files = await git.diffSummary();
								//staged
								let staged_files = await git.diffSummary([
									"--staged",
								]);

								response = {
									unadded_files: git_status.not_added,
									modified_files: modified_files.files.map(
										(file) => file.file
									),
									staged_files: staged_files.files.map(
										function (file) {
											let filename = file.file;
											let split = filename.split("/");
											let name = split.pop();
											let path = split.join("/");

											if (name.includes("{")) {
												//if you did a file rename or delete
												//staged change comes back as {old_filename => new_filename}
												//get rid of brackets
												name = name.substring(
													1,
													name.length - 1
												);
												//return second filename
												name = name.split("=>")[1];
												name = name.trim();
												return `${path}/${name}`;
											}

											return filename;
										}
									),
								};
								break;
							case "get-tags":
								let tags = await git.tags();
								response = {
									tags: tags.all,
									latest: tags.latest,
								};
								break;
							case "get-categories":
								response = settings.categories;
								break;
						}

						current_panel.webview.postMessage({
							command: message.command,
							data: response,
						});
						vscode.window.showInformationMessage(
							"Git Clerk: File List Updated"
						);
						return;
					}

					if (
						[
							"stage-file",
							"unstage-file",
							"commit-files",
							"close-window",
							"view-diff",
							"add-tag",
						].includes(message.command)
					) {
						switch (message.command) {
							case "stage-file":
								await git.add(message.file);
								current_panel.webview.postMessage({
									command: "success",
								});
								break;
							case "unstage-file":
								await git.raw(
									"restore",
									"--staged",
									message.file
								);
								current_panel.webview.postMessage({
									command: "success",
								});
								break;
							case "commit-files":
								await git.commit(message.message);
								current_panel.webview.postMessage({
									command:
										"workbench.action.closeActiveEditor",
								});
								break;
							case "close-window":
								if (current_panel)
									vscode.commands.executeCommand(
										"workbench.action.closeActiveEditor"
									);
								break;
							case "view-diff":
								vscode.commands.executeCommand(
									"workbench.view.scm"
								);
								break;
							case "add-tag":
								vscode.commands.executeCommand("git.createTag");
								break;
						}

						vscode.window.showInformationMessage(
							`Git Clerk: Successfully Executed ${message.command
								.split("-")
								.map(function (word) {
									return (
										word.charAt(0).toUpperCase() +
										word.slice(1)
									);
								})
								.join(" ")}`
						);

						return;
					}

					vscode.window.showErrorMessage(
						"Git Clerk: Could not complete operation - are you modifying the commands?"
					);
				},
				undefined,
				context.subscriptions
			);
		}
	); //end

	context.subscriptions.push(open_clerk);
}

// this method is called when your extension is deactivated
function deactivate() {}

module.exports = {
	activate,
	deactivate,
};
